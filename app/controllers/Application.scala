package controllers

import play.Logger
import play.api.libs.json.JsObject
import play.api.libs.ws.WS
import play.api.mvc._

import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future

object Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def submit = Action.async { request =>

    val key: String = request.body.asFormUrlEncoded.get("key")(0)
    val url: String = request.body.asFormUrlEncoded.get("url")(0)

    Logger.info("key : " + key)
    Logger.info("url : " + url)

    //Fetching json from kimono
    val futureOfListOfPeople = WS.url(url)
      .withHeaders("authorization" -> key)
      .get
      .map { response =>

      //find list of people (collection1) in the kimono json
      val list = (response.json \ "results" \ "collection1").as[List[JsObject]]
      val firm = (response.json \ "name").as[String]

      (list, firm)

    }

    futureOfListOfPeople.map{ listOfPeople =>

      //map a people to it's line = the emails found on google (that are futures)
      val listOfFutureOfLines = listOfPeople._1.map { node =>

        //find firstname and lastname from kimono json to query google with it.
        val firstname = (node \ "firstname" \ "text").as[String]
        val lastname = (node \ "lastname" \ "text").as[String]
        val firm = listOfPeople._2

        //for each people, query google and parse emails in html
        getEmailsFromGoogle(firstname, lastname, firm)
      }

      // transform list of future(lines) into future of list(lines)
      Future.sequence(listOfFutureOfLines)

    //work on the list inside future, thanks to flatmap
    }.flatMap( _.map( listOfLines =>

        //concat list of lines and return the string.
        Ok( listOfLines.mkString("\n") )
      )
    )

  }

  def getEmailsFromGoogle(firstname : String, lastname : String, firm : String): Future[String] = {
    val regexUrl = "class=\"r\"><a href=\"([^\"]*)\" onmousedown=\"return rwt".r

    //find urls of google results
    WS.url("http://www.google.com/search")
      .withQueryString("q" -> (firstname + " " + lastname + " " + firm + " at email"))
      .withHeaders("User-Agent" -> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36")
      .get
      .map { response =>
        val urlList = regexUrl.findAllMatchIn(response.body)
          // get url in regex groups
        urlList.map( _ group 1 ).toList


      }.map { listOfUrl =>

        // for each url, find emails in it (as a concatenated list of emails)
        val listOfFutureOfEmails = listOfUrl.map( url =>
          getEmailsFromUrl(firstname, lastname, url)
        )

        // transform list of future(concat emails) into future of list(concat emails)
        Future.sequence(
          listOfFutureOfEmails
        )

      //work on the list inside future, thanks to flatmap
      }.flatMap(futureOfListOfEmails =>

        //concat strings of the list
        futureOfListOfEmails.map( listOfEmails => listOfEmails.mkString(" ") )

      //Prepend firstname and lastname to the concatenated list of emails
      ).map (concatEmails => firstname + " " + lastname + " " + concatEmails )

  }

  def getEmailsFromUrl(firstname : String, lastname : String, url : String) : Future[String] = {
    val regexEmail = "[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+".r

    try {

      //fetch the page
      WS.url(url)
        .withHeaders("User-Agent" -> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36")
        //      .withRequestTimeout(2000)
        .get
        .map { response =>

        //find emails
        val listEmails = regexEmail.findAllIn(response.body).toList

        //keep only those containing user or name
        listEmails.filter(email => email.contains(firstname) || email.contains(lastname))

          //concat emails of this url together
          .mkString(" ")

      }

    }catch {
      case _ => Future { "" }
    }
  }

}